# gitclean.sh

Automate purging big files and cleaning up after.

Based on this blog post:
http://naleid.com/blog/2012/01/17/finding-and-purging-big-files-from-git-history/

AUTHOR: Joseph E Edwards VIII <jedwards8th@gmail.com>
URL: https://github.com/joseph8th/gitclean
