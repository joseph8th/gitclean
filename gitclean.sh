#!/usr/bin/env bash

# gitclean.sh
#
# Automate purging big files and cleaning up after.
# Based on this blog post:
# http://naleid.com/blog/2012/01/17/finding-and-purging-big-files-from-git-history/
#
# AUTHOR: Joseph E Edwards VIII <jedwards8th@gmail.com>
# URL: https://github.com/joseph8th/gitclean


SHAS_LIST=shas_list.txt
BIG_OBJ_LIST=big_objects_list.txt
BIG_TO_SMALL_LIST=big_to_small_list.txt
OUTPUT_DIR=.
PURGE_FILE=
INPUT_PURGE_FILE=
PURGE_FAIL=
FORCE_BACKUP=
PATH_TO_REPO=
SCRIPT_DIR=
CLONE_NAME=
CLONE_DIR=

_make_shas_list() {
    echo "Saving SHAs for all files > '${OUTPUT_DIR}/${SHAS_LIST}' ..."
    git rev-list --objects --all \
        | sort -k 2 \
        > "${OUTPUT_DIR}/${SHAS_LIST}"
}

_make_big_objects_list() {
    echo "Saving list of last object SHAs, all committed files, big to small > '${OUTPUT_DIR}/${BIG_OBJ_LIST}' ..."
    git gc && git verify-pack -v .git/objects/pack/pack-*.idx \
        | egrep "^\w+ blob\W+[0-9]+ [0-9]+ [0-9]+$" \
        | sort -k 3 -n -r \
        > "${OUTPUT_DIR}/${BIG_OBJ_LIST}"
}

_make_big_to_small_list() {
    echo "Making big to small list with file info > '${OUTPUT_DIR}/${BIG_TO_SMALL_LIST}' ..."
    for SHA in `cut -f 1 -d\  < "${OUTPUT_DIR}/${BIG_OBJ_LIST}"`; do
        echo $(grep $SHA "${OUTPUT_DIR}/${BIG_OBJ_LIST}") $(grep $SHA "${OUTPUT_DIR}/${SHAS_LIST}") \
            | awk '{print $1,$3,$7}' \
            >> "${OUTPUT_DIR}/${BIG_TO_SMALL_LIST}"
    done;
}

list_big_files() {
    echo "Listing big files ..."
    [[ ! -e "$OUTPUT_DIR" ]] && echo "Output directory not found: $OUTPUT_DIR" >&2 && exit 1
    _make_shas_list
    _make_big_objects_list
    _make_big_to_small_list

    [[ $? -eq 0 ]] && echo "Success!" || echo "Failed!" >&2
}

purge_file() {
    [[ -z "$INPUT_PURGE_FILE" ]] && purge_single_file && return

    input_file="$PURGE_FILE"

    echo "Purging files in list: $input_file ..."
    while IFS='' read -r PURGE_FILE || [[ -n "$PURGE_FILE" ]]; do
        echo
        purge_single_file
        [[ ! -z "$PURGE_FAIL" ]] && printf "\nFAILED on: $PURGE_FILE\n" && return
    done < "$input_file"

    printf "\nSUCCESS purging from list!\n"
}

purge_single_file() {
    echo "Purging big file: ${PURGE_FILE} ..."
    git filter-branch $FORCE_BACKUP --prune-empty --index-filter \
        "git rm -rf --cached --ignore-unmatch ${PURGE_FILE}" \
        --tag-name-filter cat -- --all

    if [[ $? -eq 0 ]]; then
        echo "Success!"
    else
        echo "Failed!" >&2
        PURGE_FAIL=1
    fi
}

git_gc() {
    echo "Compressing and pruning ..."
    git gc --prune=now --aggressive
}

clone_repo() {
    [[ "$PATH_TO_REPO" == "$CLONE_NAME" ]] && echo "Can't clone into the original repo!" && exit_safe 1
    echo "Cloning the repo to '$CLONE_NAME': $PATH_TO_REPO"
    git clone --no-hardlinks "$PATH_TO_REPO" $CLONE_NAME
}

size_repos() {
    du -h -s "$PATH_TO_REPO" "$CLONE_DIR"
}

print_help() {
    echo "usage: gitclean.sh /path/to/repo"
    printf "\t[-l /output/path] |\n\t[-p file_to_purge] [-f] [-i] |\n"
    printf "\t[-c clone] |\n\t[-s /path/to/clone] |\n\t[g]\n"
    printf "\n-l /output/path\tlist files by SHA ordered big to small\n"
    printf "-p file_to_purge\tname of file to purge from list generated by -l\n"
    printf "-f\tforce rewrite of backup (OPTIONAL: use after 1st use of -p)\n"
    printf "-i\ttreat file_to_purge as input file with list of files to purge\n"
    printf "-g\trun git gc with aggressive prune before clone\n"
    printf "-c\tclone repo after purge, no hardlinks\n"
    printf "-s\tcompare size of given repo with that of clone"
}

exit_safe() {
    cd "$SCRIPT_DIR"
    exit $1
}

main() {
    SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

    [[ "$1" == "-h" ]] && print_help

    [[ -z "$1" ]] && echo "Missing path to repo" >&2 && print_help && exit_safe 1
    PATH_TO_REPO=$(readlink -f "$1") && shift
    [[ ! -e "$PATH_TO_REPO/.git" ]] && echo "Not a git repository: $PATH_TO_REPO" && exit_safe 1

    command=print_help
    while getopts "hl:p:ifc:s:g" opt; do
        case $opt in
            l)
                [[ ! -z "$OPTARG" ]] && \
                    OUTPUT_DIR=$(readlink -f "$OPTARG") || \
                    OUTPUT_DIR=$(readlink -f "$OUTPUT_DIR")
                command=list_big_files
                ;;
            p)
                [[ -z "$OPTARG" ]] && echo "Missing file to purge" >&2 && exit_safe 1
                PURGE_FILE="$OPTARG"
                command=purge_file
                ;;
            i)
                INPUT_PURGE_FILE=1
                ;;
            f)
                FORCE_BACKUP=-f
                ;;
            c)
                [[ -z "OPTARG" ]] && echo "Missing name of clone" >&2 && exit_safe 1
                CLONE_NAME=$(readlink -f "$OPTARG")
                command=clone_repo
                ;;
            s)
                [[ -z "OPTARG" ]] && echo "Missing repo to compare size" >&2 && exit_safe 1
                CLONE_DIR=$(readlink -f "$OPTARG")
                command=size_repos
                ;;
            g)
                command=git_gc
                ;;
            h)  ;;
            *)  ;;
        esac
    done

    # Purge from list file?
    [[ ! -z "$INPUT_PURGE_FILE" ]] && PURGE_FILE=$(readlink -f "$PURGE_FILE")

    # now execute the chosen command
    cd "$PATH_TO_REPO"
    $command
}

main "$@"
exit_safe $?
